/***** ru32fi.h *****/

#ifndef __RU32FI_DEF
#define __RU32FI_DEF


#include <stdio.h>
#include <string.h>

//#include "ru32cp.h"
//#include "ru32var.h"
//#include "ru32lat.h"

extern char* Hex2Win (char* src, char* dst);
extern char* Win2Hex (char* src, char *dst);

extern void  ConvertHEX (FILE* f1, FILE* f2);
extern void  ConvertToHEX (FILE* f1, FILE* f2);

extern void TakeOddsFromFile (FILE* f1, FILE* f2);
extern void DoMime64 (FILE* f1, FILE* f2);
extern void DoBinHex (FILE* f1, FILE* f2);
extern void DoExpress (FILE* f1, FILE* f2);

extern char* TakeOddsFromString (char* src, char* dst);
extern char* StringDoMime64 (char* src, char* dst);
extern char* StringBinHex(char* src, char* dst);
extern char* StringUtf7(char* src, char* dst);
extern char* StringExpress (char* src, char* dst);
extern char* DeleteTag (char* src, char* dst);


#endif

