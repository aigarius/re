/****** ru32au.h ******/

#ifndef __RU32AU_DEF
#define __RU32AU_DEF 1

//#include "ru32cp.h"
//#include "ru32var.h"

// int ffreq[256];  //i: 0-255
// int freqblock[16];
// int bestblock[4];
// int __CpiFrom,__CpiTo;
// int uppeset,loweset,freqmax;


extern int/*bool*/ TestText(void);
extern int/*bool*/ isLatin(void);
extern int/*bool*/ isOdd(void);
extern int/*bool*/ nInBest(int iblock, int count);

extern int/*bool*/ GetTestRes(char* m_res, int *CpimFrom, int *CpimTo);

extern int/*bool*/ TestKoi8(void);
extern int/*bool*/ TestLatin(void);
extern float SampleMean (char* src);
extern int SampleMin (char* src);
extern int SampleMax (char* src);
extern void SelectBestBlocks (int BaseBlock); // BaseBlock: 0 - 8
extern int SymbolDistribution (FILE *f1);

extern void  GetFreqs (int* toffreq, int* tofreqblock,
             int *uppeset0, int *loweset0, int *freqmax0);

extern void  PutFreqs (int* fromffreq, int* fromfreqblock,
             int uppeset0, int loweset0, int freqmax0);

extern void  SymbolDistribution_for_Try (int *ffreq0, int* freqblock0,
       char* CpiTry);

#endif

