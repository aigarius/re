
/***** re_cmd.c *****/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ru32un.h"
#include "ru32call.h"

main(int argc, char* argv[])
{
// usage:
// re_cmd [opts] <filename-from> <filename_to> <cp-from> <cp_to> <s/f/d> <u/l/s>
// opts:  -[v|n][E|R|N][e|s]\n",
//  <s/f/d>: s or d(dbf) or f, default-f>
//  <u/l/s>: -u<fname>,-l<fname>,-s<fname> : 
// load user cpi, load new addcpi list, save addcpi list.

char fnfrom[81]="noname.txt",fnto[81]="noname2.txt",userfile[81],
  cpifrom[61]="Win",cpito[61]="Koi";
int cp1=-1,cp2=-1,isExt=false,isVer=true,isopt =0;
char isString = 'f', isUserFile = ' ';
TPH isPH =phNo;
   //typedef enum TPH {phRE,phER,phNo};

char src[257],dst[257], dst1[15], dst2[15], sf1[15],sf2[15];

InitLibrary();

if (argc<2)
 {
 __re_cmdprint();
 exit(0);
 }

strncpy(src,__ru32libinfo,256);

if (argv[1][0]=='-') isopt =1;
  
if (argc>6+isopt)
  { if (strlen(argv[6+isopt])>2)
     { isUserFile = argv[6+isopt][1];
       strncpy (userfile, (argv[6+isopt])+2, 80);
     } 
   // else isUserFile = ' ';
  }
if (argc>5+isopt) isString = (argv[5+isopt])[0];
if (isString != 's'  &&  isString !='d') isString = 'f';
if (argc>4+isopt) strncpy(cpito,    argv[4+isopt], 60);
if (argc>3+isopt) strncpy(cpifrom,  argv[3+isopt], 60);
if (argc>2+isopt) strncpy(fnto,     argv[2+isopt], 80);
if (argc>1+isopt) 
  { /*if (isString=='s') strncpy(src,  argv[1+isopt], 256);
    else*/               strncpy(fnfrom,argv[1+isopt], 80);
  }

if (isopt==1)
  { isVer = !(re4pos('n',argv[1])>-1);
    
    if(re4pos('E',argv[1])>-1) isPH=phRE;
    else if(re4pos('R',argv[1])>-1) isPH=phER;
    if(re4pos('N',argv[1])>-1) isPH=phNo;
    
    isExt = !(re4pos('s',argv[1])>-1);
  }
  // 11-Feb-2000 by DLL
  bool sameFile = false;
  if (!strcmp(fnfrom,fnto) && strlen(fnfrom)>0)
  {
      sameFile=true;
      strcat(fnto,".re-tmp");
  }
  // End of changes
  
  __re_cmd (fnfrom, fnto, cpifrom, cpito, isString, isUserFile, userfile,
  isExt, isPH, isVer);
  
  // 11-Feb-2000 by DLL
  if (sameFile)
  {
      remove (fnfrom);
      rename (fnto,fnfrom);
  }
  remove ("_$tmpspc.tmp");
  // End of changes
}
 
