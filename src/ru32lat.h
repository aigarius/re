/*****  ru32lat.h *****/

#ifndef __RE32LAT_DEF
#define __RE32LAT_DEF 1


void InitLatinTable (void);
void ReplaceSubWord (char* gr1, char* gr2, char* ssource);
void SubstFromLat (char* LatinTable, char* trg);
void SubstToLat (char* LatinTable, char* trg);
void CreateStandardLatin (void);
void CreateHtmlTable (void);
void WriteToTable (char* src);
void ReadFromTable(char* gr1, char* gr2);
void SaveLatinFile(char* myfilename);
void LoadLatinFile(char* myfilename);

/* for interface:
procedure ExposeCurrentInMemo(my_latin : TStrings);
procedure ApplyLatinMemo(my_latin : TStrings);
procedure LoadEditLatinFile(my_latin :TStrings; myfilename :String);
procedure SaveEditLatinFile(my_latin :TStrings; myfilename :String);
*/



extern  char* LatinTable;
extern  char* HtmlTable;
//extern  int /*bool*/  isStandardLatin;
//extern  char CurrentLatin[];
//extern  char EditLatin[];
//extern  int /*bool*/  isStandardEdit;

//extern  char* StandardLatin = "__stdlat.txt";

#endif

