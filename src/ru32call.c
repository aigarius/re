
/***** ru32call.c *****/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ru32un.h"
#include "ru32cp.h"
#include "ru32var.h"
#include "ru32mau.h"


extern void  __re_cmdprint(void);
extern void UserProcessFile(char* fnfrom, char*fnto, char*cpifrom, char* cpito);
extern void UserProcessString(char* fnfrom, char*fnto, char*cpifrom, char* cpito);
extern void __re_cmd (char* fnfrom, char* fnto, char* cpifrom, char* cpito,
   char isString, char isUserFile, char* userfile,
   int/*bool*/ isExt, TPH isPH, int/*bool*/ isVer);


// usage:
// re_cmd [opts] <filename-from> <filename_to> <cp-from> <cp_to> <s/f/d> <u/l/s>
// opts:  -[v|n][E|R|N][e|s]\n",
//  <s/f/d>: s or d(dbf) or f, default-f>
//  <u/l/s>: -u<fname>,-l<fname>,-s<fname> : 
// load user cpi, load new addcpi list, save addcpi list.

extern void  __re_cmdprint(void)
{
 char dst[81], dst1[15], dst2[15], sf1[15],sf2[15];
 int i,j,ncp,ncp1,ncp2;

 printf("\n%s\n\n",__ru32libinfo);

 printf(" usage:\n  %s\n\n",
"re options <filename-from> <filename_to> <cp-from> <cp_to> <s/d/f> <u/l/s>");

 printf("\n %s %s %s %s %s %s %s %s",
"options:  [-v][-E|-R|-N][-e|-s]\n",
"   -v - tells what is processing\n",
"   -n - don't tells what is processing (default) \n",
"   -E - converts all p,H from Russian to English\n",
"   -R - converts all p,H from English to Russian\n",
"   -N - lets all p,H Russian or English as in text (default)\n",
"   -e - converts all symbols 0x80 - 0xFF\n",
"   -s - converts only 64 symbols of Russian Alphabet (default)\n\n"
);

 printf("<cp-from>,<cp_to> - any from list (default - W,K):\n");
 ncp=GetCodepagesMaxCount();
 ncp1 = ncp/2;  ncp2=ncp%2;
 for (i=0; i<ncp1 +ncp2; i++)
 { 
   strncpy(dst1,GetCodepageByNumber(i),12);
   j = strlen(dst1);
   if ( re4pos(dst1[0],"-_<%\\") >= 0 ) j += 4; 
   strncat (dst1,"            ", 12-j);
   if (i<ncp1 || ncp2==1) strncpy(dst2,GetCodepageByNumber(i +ncp1+ncp2),12);
   else strcpy(dst2," ");

   if ( re4pos(dst1[0],"-_<%\\") < 0 )    strcpy (sf1,"  %c - %s"); 
   else if ( re4pos(dst1[0],"\\") < 0 ) strcpy (sf1," \"%c\"    - %s");
   else                                 strcpy (sf1," \"\\%c\"   - %s");

   if (dst2[0]==' ')  strcpy(sf2,"%c%s");
   else if ( re4pos(dst2[0],"-_<%\\") < 0 )    strcpy (sf2,"  %c - %s\n"); 
   else if ( re4pos(dst2[0],"\\") < 0 ) strcpy (sf2," \"%c\"    - %s\n");
   else                                 strcpy (sf2," \"\\%c\"   - %s\n");

   strcpy(dst,sf1); strcat(dst,"   "); strcat(dst,sf2);
   printf (dst,dst1[0],dst1,dst2[0],dst2);
 }
 printf (" %s\"%c\" - %s","if <cpifrom>=",'?',"try autoconvert\n\n");

 printf("%s%s%s","For <cpifrom>,<cpito> only first symbol significant,",
        " case sensitive;\n",
 " autoconvert option disabled for <s/d/f>=s or d.\n\n\n"
 );

 printf ("decrypt or encrypt file-from into file-to:\n");
 printf(" cpi-from or cpi-to = \"!\",\n");
 printf(" using  arbitrary string in other cpi as passwd.\n\n\n");

 printf("%s %s %s %s %s",
 "<s/d/f>: s(string) or d(dbf) or f(file), default-f : \n",
 "  if file - convert text-file filename-from -> filename-to\n",
 "  if dbf - convert all text fields in DBFfile filename-from -> filename-to\n",
 "  if string - convert string <filename-from> and print result to screen\n",
 "     (if string  --  arg value <filename-to> not significant).\n\n\n"
 );

 printf("%s %s %s %s",
 "<u/l/s>: -u<fname> or -l<fname> or -s<fname> or none, default-none :\n",
 "  if -u  - load user cpi from file <fname>\n",
 "  if -l  - load new addcpi set from file <fname>\n",
 "  if -s  - save current addcpi set as file <fname>.\n\n\n"
 );

}


extern void UserProcessString(char* fnfrom, char*fnto, char*cpifrom, char* cpito)
{
 if (strcmp(cpito,"--m")==0)
     MacroProcess (fnfrom, fnto, cpifrom, 's');
 else
     ProcessString (fnfrom, fnto);
}


extern void UserProcessFile(char* fnfrom, char*fnto, char*cpifrom, char* cpito)
{
//printf ("call cpito=(==,!=--m)\"%s\" (%i,%i)", cpito,strcmp(cpito,"--m"),strcmp(cpito,"-m"));

 if (cpifrom[0]=='?')
    { SetAuto2Cpi (GetCPIbyName(cpito));
      AutoProcessFile (fnfrom, fnto);
    }
 else if (strcmp(cpito,"--m")==0)
     MacroProcess (fnfrom, fnto, cpifrom, 'f');
 else
     ProcessFile (fnfrom, fnto);
}



extern void __re_cmd (char* fnfrom, char* fnto, char* cpifrom, char* cpito,
   char isString, char isUserFile, char* userfile,
   int/*bool*/ isExt, TPH isPH, int/*bool*/ isVer)
{
 int cp1=-1,cp2=-1,resUnicode;
 char sf1[15];
 
 SetisExtended (isExt);
 SetVerbose (isVer);
 SetPh (isPH);

 cp1=GetCPIbyName(cpifrom);
 cp2=GetCPIbyName(cpito);

 SetCp1(cp1);
 SetCp2(cp2);

 if (isUserFile=='u')
  { LoadUserAlphFile(userfile);
    if(isVer) printf ("**** Load User Cpi from <%s>\n", userfile);
  }
 else if (isUserFile=='l')
  { LoadAddCPI(userfile);
    if(isVer) printf ("**** Load Additional Cpi set from <%s>\n", userfile);
  }
 else if (isUserFile=='s')
  { SaveAddCPI(userfile);
    if(isVer) printf ("**** Save Additional Cpi set as  <%s>\n", userfile);
  }
//  /*otl  printf("isUserFile **%c**\n",isUserFile);


 if (cpito[0]=='!')
  {
   EncryptFile(fnfrom,fnto,cpifrom);
   if(isVer) printf ("\n**** encrypt  %s -> %s\n",fnfrom,fnto); 
  }
 else if (cpifrom[0]=='!')
  {
   DecryptFile(fnfrom,fnto,cpito);
   if(isVer) printf ("\n**** decrypt  %s -> %s\n",fnfrom,fnto); 
  }
 else if (isString=='s')
  {
  // strcpy (src, fnfrom);
   UserProcessString(fnfrom,fnto,cpifrom,cpito); // src,dst
   if(isVer)
    printf ("\n**** string %s (%s:%i) -> %s (%s:%i)\n",fnfrom,cpifrom,cp1,
            fnto,cpito,cp2);   // ..src,cpifrom,cp1,dst,cpto,cp2
  }
 else if (isString=='d')
  {
   SetDBF (true);
   ProcessFile(fnfrom,fnto);
   if(isVer)
    printf ("\n**** dbf %s (%s:%i) -> %s (%s:%i)\n",fnfrom,cpifrom,cp1,
      fnto,cpito,cp2);
  }
 else
  {
  if (cpifrom[0]=='?')
      printf("Trying autoconvert..\n");

  resUnicode = CheckUnicodeFile(fnfrom,"__tempre.txt");
  if (resUnicode == 0)
        UserProcessFile(fnfrom,fnto,cpifrom,cpito);
  else  if (resUnicode == 1)
     {
        UserProcessFile("__tempre.txt",fnto,cpifrom,cpito);
        remove ("__tempre.txt");  
     }

  if (cpifrom[0]=='?') strcpy (cpifrom, GetSelMac());
  cp1=GetCp1(); cp2=GetCp2();
  if(isVer)
    printf ("\n**** %s (%s%s:%i) -> %s (%s:%i)\n",
     fnfrom,(resUnicode == 1)?"Unicode+":"",cpifrom,cp1,
      fnto,cpito,cp2);
  }

}

