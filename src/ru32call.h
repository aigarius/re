
/***** ru32call.h *****/


extern void  __re_cmdprint(void);

extern void UserProcessFile(char* fnfrom, char*fnto, char*cpifrom, char* cpito);
extern void UserProcessString(char* fnfrom, char*fnto, char*cpifrom, char* cpito);

extern void __re_cmd (char* fnfrom, char* fnto, char* cpifrom, char* cpito,
   char isString, char isUserFile, char* userfile,
   int/*bool*/ isExt, TPH isPH, int/*bool*/ isVer);
 

// re_cmd [opts] <filename-from> <filename_to> <cp-from> <cp_to> <s/f/d> <u/l/s>
// opts:  -[v|n][E|R|N][e|s]\n",
//  <s/f/d>: s or d(dbf) or f, default-f>
//  <u/l/s>: -u<fname>,-l<fname>,-s<fname> : 
// load user cpi, load new addcpi list, save addcpi list.

