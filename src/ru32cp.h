/******   ru32cp.h  ******/


#ifndef __RU32CP_DEF
#define __RU32CP_DEF 1

#include <ctype.h>
#include <string.h>

typedef char Tcpi[4][65];

extern Tcpi CPI[];
extern char uni_dead [];
extern char CPILetter[];

extern int  CPIcount,
     CPI_ADDcount ;
extern char  *mim ;
extern char  hexsmb ;
extern int   WinCPI,PolCPI,HexCPI,OddCPI,MimeCPI,ExpressCPI,UserCPI,HtmCPI,XxCPI,UuCPI,
      BinhexCPI,Utf7CPI;

extern int  CPImax;
extern int  CPI_STANDcount;
extern int  CPIadd_defcount;
extern int  CPIaddNoChange; // uue,xxe,%hex,\hex,graph_win,<binhex>,UTF7,C,X,Y,F
extern char  macrodivlet;

extern int  CPIname;
extern int  CPIbody;
extern int  CPIBodyWin;
extern int  CPIBodyDos;
extern int  ExtCPIwin;
extern int  ExtCPIdos;

extern int  pLocation;
extern int  HLocation;

extern char *dubl_auto;
extern char *no2cpi;

extern char *mim64;
extern char *mimuu;
extern char *mimxx;
extern char *mimbin;

extern int re4pos(char ccwhat, char *sswhere);
extern int GetCPIbyName (char *myPage);
extern void InitCPIconst (void);
extern void InitCPIdefines (void);
extern void InitCPIExtended (void);
extern  /*Boolean*/ int no2this(int Cp2);


extern int GetCodepagesCount(void);
extern int GetCodepagesMaxCount(void);
//extern int GetWinCPI(void);
//extern int GetUserCPI(void);
extern const char* GetCodepageByNumber (int pnum);
extern const char* GetDublAuto(void);

#endif


