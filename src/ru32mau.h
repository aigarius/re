/******  ru32mau.h ******/

#ifndef __RU32MAU_DEF
#define __RU32MAU_DEF

//#include "ru32cp.c"
//#include "ru32var.c"
//#include "ru32fi.c"
//#include "ru32au.c"

extern int  AutoProcessFile (char* fn1, char* fn2);
extern int  Try_File (char* fn1, char* fn2);
// 0 -success, 1 -autocpi not detected, 2 -file operations failed
extern int/*bool*/  Try_File0 (FILE* f1, FILE* f2);
extern int/*bool*/ TestAutoLatin (int/*bool*/ istmp, char* m_res);
extern int   TestXX (int qcpi);
extern int/*bool*/  TryByMacros (char* m_string);
extern int/*bool*/  TestWin (void);
extern int/*bool*/  TestWinPlainTxt (void);
extern int/*bool*/  ConvertByBaseMacros (FILE *f1, FILE *f2, char *m_string);

extern int  MacroProcess (char* fn1, char* fn2, char* m_string, char isString);
  // isString: 's' or 'f', default: f
  // returns: 0-success, 1-convertion error, 2-file operation error

extern void GetAutoCpi (int *Cp1, int *Cp2);
extern const char* GetSelMac (void);



#endif

