/***** ru32var.c *****/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "ru32cp.h"

#define LF 0xa

#ifndef __RU32VARTP_DEF
#define __RU32VARTP_DEF 1

typedef enum TRul {norules,nochars,both};
// flag for Pol2Win,Win2Pol - convert char2char only,
//   convert with rules only, convert both with rules & char2char


typedef enum TPH {phRE,phER,phNo};
typedef struct {
  TPH              PH;
  int /*boolean*/  isDBF;
  int              Cp1,Cp2;
  int /*boolean*/  autocpi;
} TAttr;

#endif


  TAttr   Attr;
  TRul    __latrules;

int /*Boolean*/ isAddList, isExtended, isVerbose ;
int /*boolean*/ htmlbreak, delhtmltag, binhextag;
int             skipbyte, mimestr;
int auto2Cpi, DeepAuto;


extern void SetPh (TPH myPh);
extern void SetDBF (int /*bool*/ myDbf);
extern void SetCp1 (int myCp1);
extern void SetCp2 (int myCp2);
extern int GetCp1 (void);
extern int GetCp2 (void);
//extern void SetFn1 (char* myFn1);
//extern void SetFn2 (char* myFn2);
extern void SetAuto (int /*bool*/ autodetect);
extern void SetAuto2Cpi (int myauto2Cpi);
extern int GetAuto2Cpi (void);
extern void SetDeepAuto (int myDeepAuto);
extern int GetDeepAuto (void);
extern void SetisExtended (int myisExtended);
extern int GetisExtended (void);
extern void SetVerbose (int /*bool*/ myisVerb);
extern int GetVerbose (int /*bool*/ myisVerb);
extern void SetisAddList (int /*bool*/ myAddList);
extern int GetisAddList (void);
extern void SetDelHtmlTag (int /*bool*/ myDelTag);
extern int /*bool*/ GetDelHtmlTag(void);

extern void re4ShowMessage (char* msg);
void getline (char *str, FILE *fp1);
void putline (char *str, FILE *fp2);

extern void  re4cpfile (FILE* f1, FILE* f2);
extern int re4cpnfile (char* ffrom, char* fto); 

extern char tolowerANSI (char cc);
extern char toupperANSI (char cc);
extern long filterfile (char *xx, FILE *f1);

// ---------------------------- //

extern void SetPh (TPH myPh) { Attr.PH =myPh; };
extern void SetDBF (int /*bool*/ myDBF) { Attr.isDBF =myDBF; };
extern void SetCp1 (int myCp1) { Attr.Cp1 =myCp1; };
extern void SetCp2 (int myCp2) { Attr.Cp2 =myCp2; };
extern int GetCp1 (void) { return Attr.Cp1; };
extern int GetCp2 (void) { return Attr.Cp2; };
extern void SetAuto (int /*bool*/ autodetect) { Attr.autocpi =autodetect; };
extern void SetAuto2Cpi(int myauto2Cpi) { auto2Cpi = myauto2Cpi; };
extern int GetAuto2Cpi(void) { return  auto2Cpi; };
extern void SetDeepAuto (int myDeepAuto) { DeepAuto =myDeepAuto; };
extern int GetDeepAuto (void) { return DeepAuto; };
extern void SetisExtended (int myisExtended) { isExtended =myisExtended; };
extern int GetisExtended (void) { return isExtended; };
extern void SetVerbose (int /*bool*/ myisVerb) { isVerbose =myisVerb; };
extern int GetVerbose (int /*bool*/ myisVerb) { return isVerbose; };
extern void SetisAddList (int /*bool*/ myAddList)
 { /*Attr.addCpiList =myAddList;*/ isAddList =myAddList;
   if (myAddList)  CPIcount =CPI_STANDcount+CPI_ADDcount;
   else CPIcount =CPI_STANDcount;
 };
extern int GetisAddList (void) { return isAddList; };

extern void SetDelHtmlTag (int /*bool*/ myDelTag) { delhtmltag = myDelTag; };

extern int /*bool*/ GetDelHtmlTag (void) { return delhtmltag; };


extern void re4ShowMessage (char* msg)
{
 printf ("\n************\n** %s **\n************\n",msg);
}


void getline (char *str, FILE *fp1)
  {
  int c;
  char *p = str;
  while ( c = getc(fp1), (c != LF) && (c != EOF) )
        if (c!=0x0d) *p++ = c;
  /* *p++ = c; */  *p = '\0';
  }

void getnline (char *str, FILE *fp1, int nbytes)
  {
  int c, i=0;
  char *p = str;
  while ( c = getc(fp1), (c != LF) && (c != EOF) && (i++ < nbytes) )
        if (c!=0x0d) *p++ = c;
  if (c!=LF && c!=EOF) *p++ = c;  *p = '\0';
  }

void putline (char *str, FILE *fp2)
  {
  char *c = str, cc;
  while (*c != '\0' /* && *c != 0xa && *c != 0xd */)   putc(*c++, fp2);
/*otl printf ("%c", *c); printf ("\n"); */
  cc = 0x0d; putc(cc, fp2);
  cc = 0x0a; putc(cc, fp2);
  }

extern void  re4cpfile (FILE* f1, FILE* f2)
{
  int ch; char cc;
  while ( (ch=getc(f1)) != EOF ) { cc=ch; putc(cc,f2); }
}


extern int re4cpnfile (char* ffrom, char* fto) 
{ int i;
  remove (fto);
  i = rename (ffrom, fto);
  return i;
}


extern char tolowerANSI (char cc)
{ int i; char ci;
  i = cc;  if (i<0) i +=256;
  if (i>=0xc0 && i<0xe0) i +=32;
  if (i>=0x41 && i<0x5b) i +=0x20;
  if (i>128) i-=256;  ci=i;
  return ci; 
}

extern char toupperANSI (char cc)
{ int i; char ci;
  i = cc;  if (i<0) i +=256;
  if (i>=0xe0) i -=32;
  if (i>=0x61 && i<0x7b) i -=0x20;
  if (i>128) i-=256;  ci=i;
  return ci; 
}

extern long filterfile (char *xx, FILE *f1)
{
 int xxlen, istry, i, ch;
 long filtcount;
 char xx1st, cc;

 xxlen = strlen(xx);
 if (xxlen<1) return 0L;
 else xx1st=xx[0];

 istry = false; i = 0;
 filtcount = 0L;

 fseek (f1, 0L, SEEK_SET);

// printf ("=======filterfile-start\n"); 
 while ( (ch=getc(f1)) != EOF )
 { cc = (char) ch;
   if (!istry)
    { if (cc != xx1st) continue;
      else { i = 1; istry = true; continue; }
    }
   else
    { if (cc == xx1st)
        { i = 1; istry = true; continue; }
      else 
        if (cc != xx[i]) {istry = false; i = 0; continue; }   
        else
         { i++; if (i==xxlen) { filtcount++; istry = false; i = 0; };
         }
    }       
//  printf ("istry=%i,cc=%c,i=%i,xx[i]=%c\n",istry,cc,i,xx[i]); 
 }
//  printf ("filterfile: res= %li",filtcount); 
 return (filtcount);
}

