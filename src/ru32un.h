/***** ru32un.h *****/

// message defines:
#ifndef __ru32verinfo
#define __ru32verinfo "0.1 (4.62 port)"
#endif

#ifndef __ru32datinfo
#define __ru32datinfo "Russian Anywhere v0.1, 1999-2000"
#endif

#ifndef __ru32autinfo
#define __ru32autinfo " http//tx-tx.da.ru, http//now.at/dll"
#endif

#ifndef __ru32libinfo
#define __ru32libinfo  __ru32datinfo ", ver." __ru32verinfo "," __ru32autinfo
#endif


// header for ru32un.c:

#ifndef __RU32UN_DEF
#define __RU32UN_DEF 1

#include "ru32cp.h"
#include "ru32var.h"
#include "ru32lat.h"
#include "ru32fi.h"
//#include "ru32au.h"
//#include "ru32mau.h"


//--------------------------- RCL32 export :---------------- //

extern void InitLibrary(void);
extern char* ProcessString (char* src, char* dst);
extern int  ProcessFile (char* fn1, char* fn2);
  // returns: 0-success, 1-convertion error, 2-file operation error
extern void  ProcessFile0 (FILE* fn1, FILE* fn2);

extern int /*bool*/  CheckUnicodeFile (char*  FileSpec, char* FileNew);
 //returns: 0 if no Unicode, 1 if changed from Unicode as FileNew, 2 if failed.

extern int getDBFInfo (long *nrec, int *nfield, int *Linf, int* Lrec, char*date, FILE* f1);
// *Linf - header length (bytes number), *Lrec - record length (bytes number)
// *nrec - number of records, *nfield - number of fields,
// date - last modified (y m d - 3 bytes). 

extern void LoadUserAlph (char* myAlph);
extern void LoadUserAlphFile (char* fn);
extern void LoadAddCPI (char* fn);
extern void SaveAddCPI (char* fn);

//---(from strman:)---//

extern void  re4cpfile (FILE* f1, FILE* f2);
extern void  ConvertViaString (FILE* f1, FILE* f2);

extern char* Char2CharConvert (int cp1, int cp2, char* src);
extern void  ConvertFile (int SkipBytes, FILE* f1, FILE* f2);
extern void  ConvertDBF (FILE* f1, FILE* f2);


extern char* Pol2Win (char* LatinTable, char* src, char* dst);
extern char* Win2Pol (char* LatinTable, char* src, char* dst);

extern void  DecryptFile (char* fn1, char* fn2, char* pw);
extern void  EncryptFile (char* fn1, char* fn2, char* pw);

#endif

