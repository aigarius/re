/***** ru32var.h *****/


#ifndef __RU32VARTP_DEF
#define __RU32VARTP_DEF 1

typedef enum TRul {norules,nochars,both};
// flag for Pol2Win,Win2Pol - convert char2char only,
//   convert with rules only, convert both with rules & char2char

typedef enum TPH {phRE,phER,phNo};
typedef struct {
  TPH              PH;
  int /*boolean*/  isDBF;
  int              Cp1,Cp2;
  int /*boolean*/  autocpi;
} TAttr;

#endif


#ifndef __RU32VAR_DEF
#define __RU32VAR_DEF 1


#include <stdio.h>
//#include <string.h>
//#include <ctype.h>

//#include "ru32cp.h"

#define LF 0xa

extern  TAttr   Attr;
extern  TRul    __latrules;

extern  int /*Boolean*/ isAddList, isExtended ;
extern  int /*boolean*/ htmlbreak, delhtmltag, binhextag;
extern  int             skipbyte, mimestr;
extern  int auto_toCpi;


extern void SetPh (TPH myPh);
extern void SetDBF (int /*bool*/ myDbf);
extern void SetCp1 (int myCp1);
extern void SetCp2 (int myCp2);
extern int GetCp1 (void);
extern int GetCp2 (void);
//extern void SetFn1 (char* myFn1);
//extern void SetFn2 (char* myFn2);
extern void SetAuto (int /*bool*/ autodetect);
extern void SetAuto2Cpi(int myauto2Cpi);
extern int GetAuto2Cpi(void);
extern void SetDeepAuto (int myDeepAuto);
extern int GetDeepAuto (void);
extern void SetisExtended (int myisExtended);
extern int GetisExtended (void);
extern void SetVerbose (int /*bool*/ myisVerb);
extern int GetVerbose (int /*bool*/ myisVerb);
extern void SetisAddList (int /*bool*/ myAddList);
extern int GetisAddList (void);
extern void SetDelHtmlTag (int /*bool*/ myDelTag);
extern int /*bool*/ GetDelHtmlTag(void);

extern void re4ShowMessage (char* msg);
void getline (char *str, FILE *fp1);
void getnline (char *str, FILE *fp1, int nbytes);
void putline (char *str, FILE *fp2);

extern void  re4cpfile (FILE* f1, FILE* f2);
extern int re4cpnfile (char* ffrom, char* fto); 

extern char tolowerANSI (char cc);
extern char toupperANSI (char cc);
extern long filterfile (char *xx, FILE *f1);

#endif

