/*****  ru32lat.c *****/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "ru32var.h"

# define MAXTABLEN  10000

void InitLatinTable (void);
void ReplaceSubWord (char* gr1, char* gr2, char* ssource);
void SubstFromLat (char* LatinTable, char* trg);
void SubstToLat (char* LatinTable, char* trg);
void CreateStandardLatin (void);
void CreateHtmlTable (void);
void WriteToTable (char* src);
void ReadFromTable(char* gr1, char* gr2);
void SaveLatinFile(char* myfilename);
void LoadLatinFile(char* myfilename);

/* for interface:
procedure ExposeCurrentInMemo(my_latin : TStrings);
procedure ApplyLatinMemo(my_latin : TStrings);
procedure LoadEditLatinFile(my_latin :TStrings; myfilename :String);
procedure SaveEditLatinFile(my_latin :TStrings; myfilename :String);
*/



 char* LatinTable = NULL;
 char* HtmlTable = NULL;
 int /*bool*/  isStandardLatin;
 char CurrentLatin [81];
 char EditLatin [81];
 int /*bool*/  isStandardEdit;

 char* StandardLatin = "__stdlat.txt";

 char* CurrentTablePos;

// -------------- //

 char wrkstr_lat[257], wrkstr_lat1[257], wrkstr_lat2[257];;
   /* working string buffer */

// -------------- //

void InitLatinTable (void)
{
  CreateStandardLatin();
  CreateHtmlTable();

  strcpy (CurrentLatin, StandardLatin);
};

void ReplaceSubWord (char* gr1, char* gr2, char* ssource)
{
  int newmax, k, lengr1, lengr2;  char* cc;
//  printf ("replace- gr1,gr2: %s %s\n",gr1,gr2);

  cc = strstr (ssource, gr1);
  if (cc == NULL) return;

  lengr1 = strlen(gr1);   lengr2 = strlen(gr2);
  newmax = 256 - (lengr2 - lengr1);
  if (newmax > 256) newmax = 256;

  while (cc != NULL)
    {
      strncpy (wrkstr_lat, ssource, newmax);
      wrkstr_lat[newmax] = '\0';
      cc = strstr (wrkstr_lat, gr1);

      k = cc - wrkstr_lat;
      if (k > 0) strncpy (ssource, wrkstr_lat, k);
      ssource[k] = '\0';
      strcat (ssource, gr2);
      strcat (ssource, cc + lengr1);

      cc = strstr (ssource +k +lengr2, gr1);
    };
};


void SubstFromLat (char* LatinTable, char* trg)
{
  char  *gr1, *gr2;
// standart vars as in Win2Pol + additional variations: ju,ja etc

  gr1 = wrkstr_lat1;  gr2 = wrkstr_lat2;
  CurrentTablePos = LatinTable;

  while (*CurrentTablePos > '\1')
  {
    ReadFromTable(gr1,gr2);
    ReplaceSubWord (gr1,gr2,trg);
  };
};


void SubstToLat (char* LatinTable, char* trg)
{
  char *gr1, *gr2;

  gr1 = wrkstr_lat1;  gr2 = wrkstr_lat2;
  CurrentTablePos = LatinTable;

  while (*CurrentTablePos > '\1')
  {
    ReadFromTable(gr1,gr2);
    ReplaceSubWord (gr2,gr1,trg);
  };
};


void CreateStandardLatin (void)
{
  int tablen;
// standart vars as in Win2Pol + additional variations: ju,ja etc
  isStandardLatin = true;
  isStandardEdit  = true;

  if (LatinTable != NULL) free(LatinTable);
  LatinTable = (char*) malloc(1000);
  CurrentTablePos = LatinTable;

  WriteToTable ("BACbKA#������");
  WriteToTable ("BCE#���");
  WriteToTable ("Myppp#�����");
  WriteToTable (" eto# ���");
  WriteToTable (" ekz# ���");
  WriteToTable (" eks# ���");
  WriteToTable (" eshcho # ��� ");
  WriteToTable ("tsya#���");
  WriteToTable ("tsia#���");
  WriteToTable ("tsja#���");
  WriteToTable ("otsuts#������");
  WriteToTable ("prisuts#�������");
  WriteToTable ("molodets#�������");
  WriteToTable ("dets#����");
  WriteToTable ("Shch#�");
  WriteToTable ("Ch#�");
  WriteToTable ("Sh#�");
  WriteToTable ("Yu#�");
  WriteToTable ("Ya#�");
  WriteToTable ("Iu#�");
  WriteToTable ("Ia#�");
  WriteToTable ("Ju#�");
  WriteToTable ("Ja#�");
  WriteToTable ("Yo#�");
  WriteToTable ("YO#�");
  WriteToTable ("Zh#�");
  WriteToTable ("E\x27#�");
  WriteToTable ("shch#�");
  WriteToTable ("ch#�");
  WriteToTable ("sh#�");
  WriteToTable ("yu#�");
  WriteToTable ("ya#�");
  WriteToTable ("iu#�");
  WriteToTable ("ia#�");
  WriteToTable ("ju#�");
  WriteToTable ("ja#�");
  WriteToTable ("yo#�");
  WriteToTable ("zh#�");
  WriteToTable ("e\x27#�");
  WriteToTable ("SHCH#�");
  WriteToTable ("CH#�");
  WriteToTable ("SH#�");
  WriteToTable ("YU#�");
  WriteToTable ("YA#�");
  WriteToTable ("IU#�");
  WriteToTable ("IA#�");
  WriteToTable ("JU#�");
  WriteToTable ("JA#�");
  WriteToTable ("ZH#�");
  WriteToTable ("TSYA#���");
  WriteToTable ("TSIA#���");
  WriteToTable ("TS#�");
  WriteToTable ("H#�");
  WriteToTable ("h#�");
  WriteToTable ("ts#�");
  WriteToTable ("^#�");
  WriteToTable ("^#�");
  WriteToTable ("\x27#�");
  WriteToTable ("\x27#�");

  WriteToTable ("\x01");
};


void CreateHtmlTable (void)
{
  if (HtmlTable != NULL) free(HtmlTable);
  HtmlTable = (char*) malloc(1000);
  CurrentTablePos = HtmlTable;

  WriteToTable ("&Agrave;#�");
  WriteToTable ("&Aacute;#�");
  WriteToTable ("&Acirc;#�");
  WriteToTable ("&Atilde;#�");
  WriteToTable ("&Auml;#�");
  WriteToTable ("&Aring;#�");
  WriteToTable ("&AElig;#�");
  WriteToTable ("&Ccedil;#�");
  WriteToTable ("&Egrave;#�");
  WriteToTable ("&Eacute;#�");
  WriteToTable ("&Ecirc;#�");
  WriteToTable ("&Euml;#�");
  WriteToTable ("&Igrave;#�");
  WriteToTable ("&Iacute;#�");
  WriteToTable ("&Icirc;#�");
  WriteToTable ("&Iuml;#�");

  WriteToTable ("&ETH;#�");
  WriteToTable ("&Ntilde;#�");
  WriteToTable ("&Ograve;#�");
  WriteToTable ("&Oacute;#�");
  WriteToTable ("&Ocirc;#�");
  WriteToTable ("&Otilde;#�");
  WriteToTable ("&Ouml;#�");
  WriteToTable ("&times;#�");
  WriteToTable ("&Oslash;#�");
  WriteToTable ("&Ugrave;#�");
  WriteToTable ("&Uacute;#�");
  WriteToTable ("&Ucirc;#�");
  WriteToTable ("&Uuml;#�");
  WriteToTable ("&Yacute;#�");
  WriteToTable ("&THORN;#�");
  WriteToTable ("&szlig#�");

  WriteToTable ("&agrave;#�");
  WriteToTable ("&aacute;#�");
  WriteToTable ("&acirc;#�");
  WriteToTable ("&atilde;#�");
  WriteToTable ("&auml;#�");
  WriteToTable ("&aring;#�");
  WriteToTable ("&aelig;#�");
  WriteToTable ("&ccedil;#�");
  WriteToTable ("&egrave;#�");
  WriteToTable ("&eacute;#�");
  WriteToTable ("&ecirc;#�");
  WriteToTable ("&euml;#�");
  WriteToTable ("&igrave;#�");
  WriteToTable ("&iacute;#�");
  WriteToTable ("&icirc;#�");
  WriteToTable ("&iuml;#�");

  WriteToTable ("&eth;#�");
  WriteToTable ("&ntilde;#�");
  WriteToTable ("&ograve;#�");
  WriteToTable ("&oacute;#�");
  WriteToTable ("&ocirc;#�");
  WriteToTable ("&otilde;#�");
  WriteToTable ("&ouml;#�");
  WriteToTable ("&divide;#�");
  WriteToTable ("&oslash;#�");
  WriteToTable ("&ugrave;#�");
  WriteToTable ("&uacute;#�");
  WriteToTable ("&ucirc;#�");
  WriteToTable ("&uuml;#�");
  WriteToTable ("&yacute;#�");
  WriteToTable ("&thorn;#�");
  WriteToTable ("&yuml;#�");

  WriteToTable ("\x01");

};


void WriteToTable (char* src)
{
  int i;

  strcpy (CurrentTablePos,src);
  strcat (CurrentTablePos, "\x10");  // var 1
//  strcat (CurrentTablePos, "\x13\x10");  // var 2

  CurrentTablePos += strlen(src)+1;
};


void ReadFromTable(char* gr1, char* gr2)
{
  char* posend1, *posend2, *posbeg2;
  int nbytes, maxlen;

  maxlen = strlen(CurrentTablePos); 

  posend1 = strchr (CurrentTablePos,'#');
  posend2 = strchr (CurrentTablePos,'\x10');
  posbeg2 = (posend1 != NULL ) ? posend1+1 : CurrentTablePos ;

  if (posend1 == NULL) strcpy(gr1,"______??????");
  else
    { nbytes = posend1 - CurrentTablePos;
      if (nbytes > 256) nbytes =256; 
      strncpy (gr1,CurrentTablePos,nbytes);
      gr1[nbytes] = '\0';
    }

  if (posend2 == NULL) strcpy(gr2,"______??????");
  else
    { nbytes = posend2 - posbeg2;
      if (nbytes > 256) nbytes =256; 
      strncpy (gr2,posbeg2,nbytes);
      gr2[nbytes] = '\0';
    }

  if (posend2 != NULL  &&  strlen(posend2) >0 )  
      CurrentTablePos =  posend2+1;
  else  CurrentTablePos += maxlen;
};


void SaveLatinFile(char* myfilename)
{
  FILE *f2;
  int maxlen;

  if (LatinTable == NULL) return;

  if ( (f2=fopen(myfilename,"wb")) == NULL )
  { 
    strcpy (wrkstr_lat,"can't open new file : ");
    strcat (wrkstr_lat,myfilename);
    re4ShowMessage (wrkstr_lat);
    return;
  }

  maxlen=strlen(LatinTable);
  fwrite (LatinTable,1,maxlen,f2);
  fclose (f2);
};


void LoadLatinFile(char* myfilename)
{
  FILE *f1;
  int maxlen;
  long offs;

  if ( (f1=fopen(myfilename,"rb")) == NULL )
  { 
    strcpy (wrkstr_lat,"can't open LatinTable-File : ");
    strcat(wrkstr_lat,myfilename);
    re4ShowMessage (wrkstr_lat);
   
    CreateStandardLatin ();
    strcpy (CurrentLatin, StandardLatin);
  }
  else
  {
    fseek (f1, 0L, SEEK_END);
    offs = ftell(f1);
    maxlen  =  (offs < MAXTABLEN) ? offs : MAXTABLEN;

    if (LatinTable != NULL) free(LatinTable);
    LatinTable = (char*) malloc(maxlen +1);

    fseek (f1, 0L, SEEK_SET);
    fread (LatinTable, 1, maxlen, f1);
    LatinTable[maxlen] = '\0';
    fclose (f1);

    strcpy (CurrentLatin, myfilename);
    isStandardLatin = false;
    isStandardEdit = false;
  };
};


//-----  for interface : -----//

/*
procedure LoadEditLatinFile(my_latin :TStrings; myfilename :String);
begin
  try
   my_latin.LoadFromFile(myfilename);
   EditLatin:=myfilename;
   isStandardEdit:=false;
  except
   ShowMessage ('failed');
  end;
end;

procedure SaveEditLatinFile(my_latin :TStrings; myfilename :String);
begin
  try
   my_latin.SaveToFile(myfilename);
   EditLatin:=myfilename;
  except
   ShowMessage ('failed');
  end;
end;

procedure ExposeCurrentInMemo(my_latin : TStrings);
var gr1,gr2 : string;
begin
  EditLatin:=CurrentLatin;
  my_latin.clear;

  LatinTable.Seek(0,soFromBeginning);
  while LatinTable.Position < LatinTable.Size do
  begin
    ReadFromTable(LatinTable, gr1,gr2);
    my_latin.add(gr1+'#'+gr2);
  end;
end;

procedure ApplyLatinMemo(my_latin : TStrings);
var i,n : integer;
begin
  LatinTable.clear;
  n:=my_latin.count;
  for i:=0 to n-1 do
     WriteToTable (my_latin.Strings[i]);
  CurrentLatin:=EditLatin;
  isStandardLatin:=isStandardEdit;
end;

*/
