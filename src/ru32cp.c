/******   ru32cp.c  ******/

#include <ctype.h>
#include <string.h>
#include <stdio.h>

typedef char Tcpi[4][65];

//--------------------------- CONSTNTS :---------------- //

#define POINTS64 "................................................................"

Tcpi CPI[32] =
  {
    {"Windows","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Dos","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"KOI-8","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Latin","ABVGDEGZIJKLMNOPRSTUFHCCSS^Y^EUAabvgdegzijklmnoprstufhccss^y^eua",
       POINTS64,POINTS64},
    {"Iso","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"HEX","","",""},
    {"ShiftKbrd","F<DULT:PBQRKVYJGHCNEA{WXIO}SM\">Zf,dult;pbqrkvyjghcnea[wxio]sm\".z",
       POINTS64,POINTS64},
    {"Mac","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"AFF","@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
       POINTS64,POINTS64},
    {"Odd(UTF8_1)","","",""},
    {"Base64","","",""},
    {"Express","","",""},
    {"T-Html","","",""},
    {"User","����������������������������������������������������������������",
       POINTS64,POINTS64},


    {"-uue","","",""},
    {"_xxe","","",""},
    {"%hex","","",""},
    {"\\'hex","","",""},
    {"Graph_win","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"<binhex>","","",""},
    {"+UTF7-","","",""},
    {"C_MIC","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Y_c16","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Z_c32","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"F(UTF8_2)","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Pict","A�B�DE����K�MHO�PCTY�X����������a����e����k���o�pc�y�x����������",
       POINTS64,POINTS64},
    {"N_Estl","A�B��E����K�MHO�PCT��X����������a떗�e��������o�pc���x����������",
       POINTS64,POINTS64},
    {"V_Vpp855","��쭧��������������諶���������ࠢ묦������������窵����������",
       POINTS64,POINTS64},
    {"X_sp","�?����?��?���������?�?���??????�ݰ���?�߮�����������������?�����",
       POINTS64,POINTS64},
    {"J_diff","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"Q","����������������������������������������������������������������",
       POINTS64,POINTS64},
    {"R","����������������������������������������������������������������",
       POINTS64,POINTS64}
  };

/*
���������������� -8
���������������� -9
���������������� -a
���������������� -b
���������������� -c
���������������� -d
���������������� -e
���������������� -f
*/

char uni_dead [128] =
{0x02,0x04,0x03,0x04,0x1A,0x20,0x53,0x04,0x1E,0x20,0x26,0x20,0x20,0x20,0x21,0x20
,0x88,0x00,0x30,0x20,0x09,0x04,0x39,0x20,0x0A,0x04,0x0C,0x04,0x0B,0x04,0x0F,0x04
,0x52,0x04,0x18,0x20,0x19,0x20,0x1C,0x20,0x1D,0x20,0x22,0x20,0x13,0x20,0x14,0x20
,0x98,0x00,0x22,0x21,0x59,0x04,0x3A,0x20,0x5A,0x04,0x5C,0x04,0x5B,0x04,0x5F,0x04
,0xA0,0x00,0x0E,0x04,0x5E,0x04,0x08,0x04,0xA4,0x00,0x90,0x04,0xA6,0x00,0xA7,0x00
,0x01,0x04,0xA9,0x00,0x04,0x04,0xAB,0x00,0xAC,0x00,0xAD,0x00,0xAE,0x00,0x07,0x04
,0xB0,0x00,0xB1,0x00,0x06,0x04,0x56,0x04,0x91,0x04,0xB5,0x00,0xB6,0x00,0xB7,0x00
,0x51,0x04,0x16,0x21,0x54,0x04,0xBB,0x00,0x58,0x04,0x05,0x04,0x55,0x04,0x57,0x04
};

char CPILetter[33]="";

int  CPIcount,
     CPI_ADDcount ;
char *mim ;
char  hexsmb ;
int   WinCPI,PolCPI,HexCPI,OddCPI,MimeCPI,ExpressCPI,UserCPI,HtmCPI,XxCPI,UuCPI,

      BinhexCPI,Utf7CPI;

int  CPImax = 32;
int  CPI_STANDcount = 14;
int  CPIadd_defcount = 15;
int  CPIaddNoChange = 7; // uue,xxe,%hex,\hex,graph_win,<binhex>,UTF7,C,X,Y,F
char  macrodivlet = '~';

int  CPIname = 0;
int  CPIbody = 1;
int  CPIBodyWin = 2;
int  CPIBodyDos = 3;
int  ExtCPIwin = 64;
int  ExtCPIdos = 64; // 48-pseudographics
  // + DOS #251#254 - "square root" and  "black square - end of chapter"
  // + KOI #147#151#152#153#155#157#159
  // - upper half of integral,nearly equal,less or equal,great or equal,
  //   lower half of integral,in power z,integer dividing
  // + 7 reserv
//  ExtCpiAutoLength=2;

int  pLocation = 48;
int  HLocation = 13;

char *dubl_auto = "HOBE-_\\%<T+";
char * no2cpi   ="OBE-_\\%<+";

char *mim64 ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
char *mimuu ="`!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_";
char *mimxx ="+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
char * mimbin="!\"#$%&'()*+,-012345689@ABCDEFGHIJKLMNPQRSTUVXYZ[`abcdefhijklmpqr";
// in mimuu  - quote:  chr($27) ;

extern int re4pos(char ccwhat, char *sswhere);
extern int GetCPIbyName (char *myPage);
extern void InitCPIconst (void);
extern void InitCPIdefines (void);
extern void InitCPIExtended (void);
extern  /*Boolean*/ int no2this(int Cp2);

extern int GetCodepagesCount(void);
extern int GetCodepagesMaxCount(void);
//extern int GetWinCPI(void);
//extern int GetUserCPI(void);
extern const char* GetCodepageByNumber (int pnum);
extern const char* GetDublAuto(void);

//-----------------------//

extern int re4pos (char ccwhat, char *sswhere)
{
  char *ss;
//  int i;
//  for (i=0,ss=sswhere; (*ss!='\0')&&(*ss!=ccwhat); ss++,i++);
//  if (*ss!=ccwhat) i=-1;

  ss = strchr (sswhere, ccwhat);
  return (ss != NULL) ? ss-sswhere : -1 ;
}


extern int GetCPIbyName (char *myPage)
{
char cc;
cc = (char)(toupper((int)myPage[0]));
   // /*otl:*/ printf ("myP=%s cc=%c CPILet=%s\n",myPage,cc,CPILetter);
return re4pos(cc, CPILetter); 
};


extern int /*Boolean*/ no2this(int Cp2)
{
 return ( re4pos(CPILetter[Cp2],no2cpi)>-1 );
};


extern void InitCPIconst(void)
{
  int i,c;

  mim = mim64;
  hexsmb = '=';
  CPI_ADDcount = CPIadd_defcount;
  CPIcount = CPI_STANDcount;

  c = CPI_STANDcount+CPI_ADDcount;
  for (i=0; i < c; i++)
    {
    CPILetter[i]=(char)toupper((CPI[i][CPIname])[0]);
    }
  CPILetter[c] = '\0';

    // /*otl:*/  printf("c=%i CPILetter=%s\n",c,CPILetter);
  InitCPIExtended();
  InitCPIdefines();
};


extern void InitCPIdefines(void)
{
  WinCPI = GetCPIbyName("Win"); //1;
  PolCPI = GetCPIbyName("Lat"); //4;
  HexCPI = GetCPIbyName("Hex"); //6;
  OddCPI = GetCPIbyName("Odd"); //10;
  MimeCPI = GetCPIbyName("Base64"); //11;
  ExpressCPI = GetCPIbyName("Express"); //12;
  UserCPI = GetCPIbyName("User"); //14;
  HtmCPI = GetCPIbyName("T-Html"); //;
  XxCPI = GetCPIbyName("_Xxe"); //;
  UuCPI = GetCPIbyName("-Uue"); //;
  BinhexCPI = GetCPIbyName("<Binhex>"); //;
  Utf7CPI = GetCPIbyName("+UTF7-"); //;
};


extern void InitCPIExtended()
{
 int i,c;
 char *is,*cs;

 //old:  yo YO (2 byte) + '<<','>>' (2 byte) + pseudographics (48 byte):


 c = GetCPIbyName("WIN");
 strcpy ( CPI[c][2],
// '����'; //  =A8=B8=AB=BB, i.e.  #168#184#171#187
 "����������������������������������������������������������������" );

 c = GetCPIbyName("Gr_win");
 strcpy ( CPI[c][2],
// '��<>';  
 "�������������������������������������������<���������������>����" );
 strcpy ( CPI[c][3],
 "...|-=--==|==-=------+=-======+=-=--==--=--....................." );


 c = GetCPIbyName("DOS");
 strcpy ( CPI[c][2],
 ".....................\xf9.........." );
 strcat ( CPI[c][2],
 "\xff\xf6\xf7.\xfd...\xf0.\xf2....\xf4\xf8......\xfa\xf1\xfc\xf3....\xf5" );
// '��<>'; // =F0=F1 i.e. #240#241
// '.....................'#249'..........'+
// #255#246#247'.'#253'...'#240'.'#242'....'#244#248'......'#250#241#252#243'....'#245;

 strcpy (CPI[c][3],
 "\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf" );
 strcat (CPI[c][3],
 "\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf" );
 strcat (CPI[c][3],
 "\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf" );
 strcat (CPI[c][3],
 "\xfb\xfe.............." );
// #176#177#178#179#180#181#182#183#184#185#186#187#188#189#190#191+
// #192#193#194#195#196#197#198#199#200#201#202#203#204#205#206#207+
// #208#209#210#211#212#213#214#215#216#217#218#219#220#221#222#223+
// #251#254'..............';


 c = GetCPIbyName("KOI");
 strcpy (CPI[c][2],
 ".....................\x95.........." );
 strcat (CPI[c][2],
 "\x9a.......\xb3\xbe......\x9c......\x9e\xa3......." );

// {'................................��������������������������������';
//   - according to the book } 

// '����';
// {#176#177#178#179#180#181#182#183#184#185#186#187#188#189#190#191+
// #128#129#130#131#132#133#134#135#136#137#138#139#140#141#142#143+
// #144#145#146#147#148#149#150#151#152#153#154#155#156#157#158#159;
//  - according to the book } 

 strcpy (CPI[c][3],
 "\x90\x91\x92\x81\x87\xb2\xb4\xa7\xa6\xb5\xa1\xa8\xae\xad\xac\x83" );
 strcat (CPI[c][3],
 "\x84\x89\x88\x86\x80\x8a\xaf\xb0\xdb\xa5\xbb\xb8\xb1\xa0\xbe\xb9" );
 strcat (CPI[c][3],
 "\xba\xb6\xb7\xaa\xa9\xa2\xa4\xbd\xbc\x85\x82\x8d\x8c\x8e\x8f\x8b" );
 strcat (CPI[c][3],
 "\x96\x94\x93\x97\x98\x99\x9b\x9d\x9f......." );

// #144#145#146#129#135#178#180#167#166#181#161#168#174#173#172#131+
// #132#137#136#134#128#138#175#176#219#165#187#184#177#160#190#185+
// #186#182#183#170#169#162#164#189#188#133#130#141#140#142#143#139+
// #150#148#147#151#152#153#155#157#159'.......';


 c = GetCPIbyName("LAT");
 strcpy (CPI[c][2],
 "........................................E...............e......." );


 c = GetCPIbyName("ISO");
 strcpy (CPI[c][2],
 "\xa2\xa3.\xf3........\xaa\xac\xab\xaf\xf2.....\xf3...\xf9.\xfa\xfc\xfb\xff" );
 strcat (CPI[c][2],
 "\xa0\xae\xfe\xa8...\xfd\xa1.\xa4....\xa7..\xa6\xf6....\xf1\xf0\xf4.\xf8\xa5\xf5\xf7" );

// '��<>';  //=A1=F1, i.e. #161#241
// #162#163'.'#243'........'#170#172#171#175#242'.....'#173'...'#249'.'#250#252#251#255+
// #160#174#254#168'...'#253#161'.'#164'....'#167'..'#166#246'....'#241#240#244'.'#248#165#245#247;

// { '.....................'#250'...'#249'......'+  - according to the book
// '.'#247#248'.'#254'...'#161'.'#242'....'#245'.......'#251#241#253#243'....'#246; } 
 
// CPI[c,3]:=
// { #155#156#157#165#167#131#132#133#134#151#149#145#146#139#140#160+
// #163#168#166#169#164#170#141#142#147#144#152#150#153#148#154#128+
// #129#130#135#136#137#138#143#158#159#162#160#171#172#173#174#175; - according to the book
//} 

 c = GetCPIbyName("Shift");
 strcpy (CPI[c][2],
 "........................................~...............`......." );
 // '~`��';


 c = GetCPIbyName("MAC");
 strcpy (CPI[c][2],
 "\xab\xae.\xaf\xd7\xc9\xa0...\xbc.\xbe\xcd\xcb\xda" );
 strcat (CPI[c][2],
 "\xac\xd4\xd5\xd2\xd3\xa5\xd0\xd1.\xaa\xbd.\xbf\xce\xcc\xdb" );
 strcat (CPI[c][2],
 "\xca\xd8\xd9\xb7\xcd..\xa4\xdd\xa9\xb8\xc7\xc2.\xa8\xba" );
 strcat (CPI[c][2],
 "\xa1\xb1\xa7\xb4..\xa6.\xde\xdc\xb9\xc8\xc0\xc1\xcf\xbb" );
// #171#174'.'#175#215#201#160'...'#188'.'#190#205#203#218+
// #172#212#213#210#211#165#208#209'.'#170#189'.'#191#206#204#219+
// #202#216#217#183#255'..'#164#221#169#184#199#194'.'#168#186+
// #161#177#167#180'..'#166'.'#222#220#185#200#192#193#207#187;
 // '����'; //=DD=DE=C7=C8  i.e. #221#222 #199#200
 
 strcpy (CPI[c][3], "................................................" );
 strcat (CPI[c][3],
 "\xc3...\xb2\xb3..\xd6......." );
// #195'...'#178#179'..'#214'.......';


 c = GetCPIbyName("AFF");
 strcpy (CPI[c][2],
 "........................................(...............8......." );
 // '(8��';


 c = GetCPIbyName("Pict");
 strcpy (CPI[c][2],
 "........................................E...............e......." );
 //'Ee��';


 c = GetCPIbyName("User"); //{MIC}
 strcpy (CPI[c][2],
 ".....................\xf9.........." );
 strcat (CPI[c][2],
 "\xff......\xd6........\xf8\xf1...\xe6.\xfa.\xd5......" );

 strcpy (CPI[c][3],
 "\xd0\xd1\xd2\xd3\xd4....\xc6\xc7\xd7\xd8..\xcf" );
 strcat (CPI[c][3],
 "\xc0\xc1\xc2\xc3\xc4\xc5..\xc8\xc9\xca\xcb\xcc\xcd\xce." );
 strcat (CPI[c][3],
 ".........\xd9\xda\xdb\xdc\xdd\xde\xdf" );
 strcat (CPI[c][3],
 "\xfb\xfe\xf4\xf7\xf3\xf2\xf5\xfd\xf6......." );

 //pseudographics - is exactly as in the book
// #208#209#210#211#212'....'#198#199#215#216'..'#207+
// #192#193#194#195#196#197'..'#200#201#202#203#204#205#206'.'+
// '.........'#217#218#219#220#221#222#223+
// #251#254#244#247#243#242#245#253#246'.......';


// maybe better to turn off, for not to disturbe macros processing ?
  ///*otl:*/  for (i=0; i<32; i++) printf("CP[%i][0]=%s\n",i,CPI[i][0]);
 c = GetCPIbyName("C_MIC");
 i = GetCPIbyName("User");
  ///*otl*/printf("C %i,U %i\n",c,i);
  //cs=CPI[0][0];  is=CPI[i][2];
  ///*otl:*/  printf("cs=%s\n is=%s\n",cs,is);
  // strcpy (cs, "--"/*is*/);
 strcpy (CPI[c][2], CPI[i][2] );
 strcpy (CPI[c][3], CPI[i][3] );

};

extern int GetCodepagesCount(void) { return CPIcount; } 
extern int GetCodepagesMaxCount(void) {return CPI_STANDcount+CPIadd_defcount;} 
//extern int  GetWinCPI(void) { return WinCPI; };
//extern int  GetUserCPI(void) { return UserCPI; };

extern const char* GetCodepageByNumber (int pnum)
{
  if (pnum<0 || 
      pnum> CPI_STANDcount+CPI_ADDcount)
  return "??";
  else return CPI[pnum][CPIname];
};

extern const char* GetDublAuto(void) { return dubl_auto; };



